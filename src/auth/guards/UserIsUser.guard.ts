import { CanActivate, ExecutionContext, forwardRef, Inject } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/user/models/user.interface';
import { UserService } from 'src/user/service/user.service';

export class UserIsUser implements CanActivate {

    constructor(
        @Inject(forwardRef(() => UserService))
        private userService: UserService
    ) { }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        const params = request.params;      
        const user: User = request.user;

        return this.userService.findOne(user.id).pipe(
            map((user: User) => {
                return user && user.id === params.id;
            })
        )
    }
}