import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../models/user.entity';
import { Repository } from 'typeorm';
import { User } from "../models/user.interface";
import { from, Observable } from 'rxjs';
import { map, switchMap } from "rxjs/operators";
import { CreateUserDto } from 'src/dto/CreateUser.dto';
import { AuthService } from 'src/auth/services/auth.service';
import { LoginUserDto } from 'src/dto/LoginUser.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>,
        private authService: AuthService
    ) { }

    create(createdUserDto: CreateUserDto): Observable<User> {
        return this.emailExists(createdUserDto.email).pipe(
            switchMap((email: boolean) => {
                if (!email) {
                    return this.usernamelExists(createdUserDto.username).pipe(
                        switchMap((username: boolean) => {
                            if (!username) {
                                return this.authService.hashPassword(createdUserDto.password).pipe(
                                    switchMap((hash: string) => {
                                        createdUserDto.password = hash;
                                        return from(this.userRepository.save(createdUserDto)).pipe(
                                            map((savedUser: User) => {
                                                const { password, ...user } = savedUser;
                                                return user;
                                            })
                                        )
                                    })
                                )
                            } else {
                                throw new HttpException("username is aleady in use", HttpStatus.CONFLICT);
                            }
                        })
                    )
                } else {
                    throw new HttpException("email is aleady in use", HttpStatus.CONFLICT);
                }
            })
        )
    }

    login(loginUserDto: LoginUserDto): Observable<string> {
        return this.findUserByEmail(loginUserDto.email).pipe(
            switchMap((user: User) => {
                if (user) {
                    return this.validatePassword(loginUserDto.password, user.password).pipe(
                        switchMap((passwordsMatch: boolean) => {
                            if (passwordsMatch) {
                                return this.findOne(user.id).pipe(
                                    switchMap((user: User) => this.authService.generateJwt(user))
                                )
                            } else {
                                throw new HttpException("fucksy wucksy", HttpStatus.UNAUTHORIZED)
                            }
                        })
                    )
                } else {
                    throw new HttpException("user not found", HttpStatus.NOT_FOUND);
                }
            })
        )
    }

    findOne(id: string): Observable<User> {
        return from(this.userRepository.findOne(id));
    }

    findAll(): Observable<User[]> {
        return from(this.userRepository.find());
    }

    deleteOne(id: string): Observable<any> {
        return from(this.userRepository.delete(id));
    }

    updateOne(id: string, user: User): Observable<any> {
        return from(this.userRepository.update(id, user));
    }

    private findUserByEmail(email: string): Observable<User> {
        return from(this.userRepository.findOne({ email }, { select: ["id", "username", "email", "password"] }));
    }

    private validatePassword(password: string, hash: string): Observable<boolean> {
        return this.authService.comparePasswords(password, hash);
    }

    private emailExists(email: string): Observable<boolean> {
        return from(this.userRepository.findOne({ email })).pipe(
            map((user: User) => {
                return !!user
            })
        )
    }

    private usernamelExists(username: string): Observable<boolean> {
        return from(this.userRepository.findOne({ username })).pipe(
            map((user: User) => {
                return !!user
            })
        )
    }
}
