import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { UserIsUser } from 'src/auth/guards/UserIsUser.guard';
import { CreateUserDto } from 'src/dto/CreateUser.dto';
import { LoginUserDto } from 'src/dto/LoginUser.dto';
import { User } from '../models/user.interface';
import { UserService } from '../service/user.service';

@Controller('user')
export class UserController {

    constructor(private userService: UserService) { }

    @Post()
    create(@Body() createUserDto: CreateUserDto): Observable<User> {
        return this.userService.create(createUserDto);
    }

    @Post("login")
    @HttpCode(200)
    login(@Body() loginUserDto: LoginUserDto): Observable<Object> {
        return this.userService.login(loginUserDto).pipe(
            map((jwt: string) => {
                return {
                    access_token: jwt,
                    token_type: "JWT",
                    expires_in: 3600
                }
            })
        );
    }

    @UseGuards(JwtAuthGuard)
    @Get(":id")
    findOne(@Param("id") id: string): Observable<User> {
        return this.userService.findOne(id);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    findAll(@Req() request): Observable<User[]> {
        return this.userService.findAll();
    }

    @UseGuards(JwtAuthGuard, UserIsUser)
    @Delete(":id")
    deleteOne(@Param("id") id: string): Observable<User> {
        return this.userService.deleteOne(id)
    }

    @UseGuards(JwtAuthGuard, UserIsUser)
    @Put(":id")
    updateOne(@Param("id") id: string, @Body() user: User): Observable<any> {
        return this.userService.updateOne(id, user)
    }
}
